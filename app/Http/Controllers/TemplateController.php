<?php

namespace App\Http\Controllers;

use App\Template;
use App\Checklist;
use App\Item;
use Illuminate\Http\Request;

class TemplateController extends Controller
{    

    public function index(Request $request){
        $limit = 1;
        // $sort = substr($request->sort, 0) == "-" ? str_replace('-', '', $request->sort) : $request->sort;
        // $sortBy = substr($sort, 0) == "-" ? 'desc' : 'asc';

        $templates = Template::with('checklist', 'items')
            // ->orderBy($sort, $sortBy)
            ->paginate($limit);
        // dd($templates->toArray());

        return response()->json([
            'meta' => [
                'count' => $templates->count(),
                'total' => $templates->toArray()['total']
            ],
            'links' => [
                'first' => $templates->toArray()['first_page_url'],
                'last' => $templates->toArray()['last_page_url'],
                'next' => $templates->toArray()['next_page_url'],
                'prev' => $templates->toArray()['prev_page_url'],
            ],
            'data' => $templates->toArray()['data']
        ]);
    }

    public function store(Request $request) {
        // dd($request->data['attributes']['items']);
        $Template = new Template;
        $Template->name = $request->data['attributes']['name'];
        $Template->save();

        $Checklist = new Checklist;
        $Checklist->object_domain = $request->data['attributes']['name'];
        $Checklist->description = $request->data['attributes']['checklist']['description'];
        $Checklist->object_id = $Template->id;
        $Checklist->save();

        $ItemsData = [];
        foreach($request->data['attributes']['items'] as $index => $value) {
            array_push($ItemsData, [
                'description' => $value['description'],
                'urgency' => $value['description'],
                'assignee_id' => $Checklist->id,
                'task_id' => $Template->id,
            ]);
        }
        $Items = Item::insert($ItemsData);
        // $Items->save();

        $data = [
            'id' => $Template->id,
            'attributes' => [
                'name' => $Template->name,
                'checklist' => $Checklist,
                'items' => $ItemsData
            ]
        ];

        return response()->json([
            'data' => $data
        ], 201);
    }

    public function show($id, Request $request){
        $templates = Template::with('checklist', 'items')->find($id);

        return response()->json([
            'data' => [
                'type' => 'templates',
                'id' => $id,
                'attributes' => [
                    'name' => $templates->name,
                    'items' => $templates->items,
                    'checklist' => $templates->checklist,
                ],
                'links' => [
                    'self' => $request->url()
                ]
            ]
        ], 200);
    }

    public function destroy($id){
        $templates = Template::find($id)->delete();

        return response()->json([
            'status' => 200,
            'message' => 'success'
        ]);
    }

}
