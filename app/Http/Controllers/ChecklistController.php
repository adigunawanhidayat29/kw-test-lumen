<?php

namespace App\Http\Controllers;

use App\Template;
use App\Checklist;
use App\Item;
use Illuminate\Http\Request;

class ChecklistController extends Controller
{    

    public function index(Request $request, $id){
        $checklist = Checklist::find($id);

        return response()->json([            
            'data' => [
                'type' => 'checklist',
                'id' => $checklist->id,
                'attributes' => $checklist,
                'links' => [
                    'self' => $request->url()
                ]
            ]
        ]);
    }

    public function store(Request $request) {        
        $Checklist = new Checklist;
        $Checklist->object_domain = $request->data['attributes']['object_domain'];
        $Checklist->description = $request->data['attributes']['description'];
        $Checklist->object_id = $request->data['attributes']['object_id'];
        $Checklist->due = $request->data['attributes']['due'];
        $Checklist->urgency = $request->data['attributes']['urgency'];
        $Checklist->save();

        $ItemsData = [];
        foreach($request->data['attributes']['items'] as $index => $value) {
            array_push($ItemsData, [
                'description' => $value,
                'urgency' => $request->data['attributes']['urgency'],
                'assignee_id' => $Checklist->id,
                'task_id' => $request->data['attributes']['task_id'],
            ]);
        }
        $Items = Item::insert($ItemsData);
        // $Items->save();

        $data = [
            'type' => 'checklist',
            'id' => $Checklist->id,
            'attributes' => $Checklist,
            'links' => [
                'self' => url('checklists/' . $Checklist->id)
            ]
        ];

        return response()->json([
            'data' => $data
        ], 201);
    }
}
