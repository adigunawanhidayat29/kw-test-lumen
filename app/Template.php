<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    protected $hidden = ['created_at', 'updated_at'];

    public function checklist() {
        return $this->hasOne('App\Checklist', 'object_id')->select(['object_id','description', 'due']);
    }

    public function items() {
        return $this->hasMany('App\Item', 'task_id')->select(['task_id','description', 'urgency', 'due']);
    }
}
