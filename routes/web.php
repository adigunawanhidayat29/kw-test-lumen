<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/checklists/templates', 'TemplateController@index');
$router->post('/checklists/templates', 'TemplateController@store');
$router->get('/checklists/templates/{id}', 'TemplateController@show');
$router->delete('/checklists/templates/{id}', 'TemplateController@destroy');

$router->get('/checklists/{checklistId}', 'ChecklistController@index');
$router->post('/checklists', 'ChecklistController@store');
