<?php

class ChecklistTest extends TestCase
{    

    /**
     * /checklists/id [GET]
     */
    public function testShouldReturnChecklist(){
        $this->get("checklists/1", []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure(
            [
                'data' => [
                    'type',
                    'id',
                    'attributes',
                    'links' => [
                        'self'
                    ]
                ]
            ]    
        );
    }

    /**
     * /checklists [POST]
     */
    public function testShouldCreateChecklist(){
        $parameters = [
            'data' => [
                'attributes' => [
                    'object_domain' => 'contact',
                    'object_id' => '1',
                    'due' => '2019-01-25T07:50:14+00:00',
                    'urgency' => 1,
                    'description' => 'Need to verify this guy house.',
                    'items' => [
                        "Visit his house",
                        "Capture a photo",
                        "Meet him on the house"
                    ],
                    "task_id" => 123
                ]
            ]
        ];
        $this->post("/checklists", $parameters, []);
        $this->seeStatusCode(201);
        $this->seeJsonStructure(
            ['data' =>
                [
                    'type',
                    'id',
                    'attributes',
                    'links' => [
                        'self'
                    ]
                ]
            ]    
        );
    }
}
