<?php

class TemplateTest extends TestCase
{
    /**
     * /checklists/templates/ [GET]
     */
    public function testShouldReturnAllTemplates()
    {
        $this->get("/checklists/templates", []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'meta' => [
                'count',
                'total',
            ],
            'links' => [
                'first',
                'last',
                'next',
                'prev',
            ],
            'data' => [
                [
                    'name',
                    'checklist',
                    'items'
                ]
            ],
            
        ]);
    }

    /**
     * /checklists/templates/ [POST]
     */
    public function testShouldCreateTemplate(){
        $parameters = [
            'data' => [
                'attributes' => [
                    'name' => 'test',
                    'checklist' => [
                        'description' => 'test decription'
                    ],
                    'items' => [
                        [
                            'description' => 'test item desc',
                            'urgency' => '2',
                        ],
                        [
                            'description' => 'test item desc',
                            'urgency' => '2',
                        ]
                    ]
                ]
            ]
        ];
        $this->post("/checklists/templates", $parameters, []);
        $this->seeStatusCode(201);
        $this->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'attributes' => [
                        'name',
                        'checklist',
                        'items',
                    ]
                ]
            ]    
        );
    }

    /**
     * /checklists/templates/id [GET]
     */
    public function testShouldReturnTemplate(){
        $this->get("checklists/templates/9", []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure(
            [
                'data' => [
                    'type',
                    'id',
                    'attributes' => [
                        'name',
                        'items',
                        'checklist',
                    ],
                    'links' => [
                        'self'
                    ]
                ]
            ]    
        );
    }

    /**
     * /checklists/templates/id [DELETE]
     */
    public function testShouldDeleteTemplate(){
        
        $this->delete("checklists/templates/9", [], []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'status',
            'message'
        ]);
    }
}
